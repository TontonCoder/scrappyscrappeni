
#!/usr/bin/python
# -*- coding: utf-8 -*-
from modules.image import Image
from modules.Data import Data
from modules.Parser import Parser
from modules.Resource import Resource
from modules.Bot import Bot
from modules.Book import Book
from modules.Chapter import Chapter
import getpass

class Scrappy:
    def __init__(self):
        self.bot = Bot()
        self.data = Data()
        self.parser = Parser()
        self.username = None
        self.password = None
        self.cookies = None
        self.checkData()
        self.book = None
        self.link = None

    def checkData(self):
        print("Checking data")
        self.cookies = self.data.getCookies()
        userdata = self.data.getUser()

        if(userdata != None):
            self.username = userdata[0]
            self.password = userdata[1]
            print("Logins look like OK! - "+self.username)

        if userdata == None:
            print("No Logins found")

    def requestCookiesConnect(self):
        self.bot.connect(self.username, self.password)
        self.cookies = self.bot.extractCookies()
        self.data.saveCookies(self.cookies)

    def saveCredentials(self,_username,_password):
        self.data.saveUser(_username, _password)

    def setCredentials(self, _username,_password):
        self.username = _username
        self.password = _password

    def credentialsOk(self):
        if(self.username == None):
            return False
        if(self.password == None):
            return False

    def createBook(self, name, url,chapterLst):
        self.book = Book(url,name,chapterLst)

    def createChapter(self,id,title,preHtml,postHtml,imgs):
        _chapter = Chapter()
        _chapter.id = id
        _chapter.name = title
        _chapter.preHtml = preHtml
        _chapter.postHtml = postHtml
        _chapter.Images = imgs
        self.book.addChapter(_chapter)
         
    def cookiesAvailable(self):
        return self.cookies != None

    def checkUserExists(self):
        if(scrappy.credentialsOk() == False):
            pass

    def enterUserCredentials(self):
        u = input("Enter username:")
        print("Username entered:"+u)
        p = getpass.getpass("Enter password:\n")
        self.saveCredentials(u,p)

    def scrapLink(self, link):
        self.link = link
        self.bot.injectCookies(self.cookies)
        self.bot.startscrapping(self.link)
        #Getting main content
        currentContent = self.bot.getContent()
        self.parseInfo()
        #Parsing main content

        for i in range(len(self.book.chaptersList)):
            content = self.getMainContent()
            cleanedContent = self.cleanHtml(content)
            print(str(cleanedContent))
            imgs = self.parser.extractImgs(cleanedContent)
            postcontent = self.parser.updateImgSrc(cleanedContent)
            print("imgs:"+str(imgs)+"-"+ str(len(imgs)))
            self.createChapter(i,self.book.chaptersList[i], cleanedContent,postcontent, imgs)
            self.next()

        self.saveBookHtml()

    def parseInfo(self):
            title = self.bot.getTitle()
            summary = self.bot.getSummary()
            lstChapters = self.parser.getChapterList(summary)
            numberOfChapters = self.parser.getChapterNumbers(summary)
            print("Number of chapters:"+str(numberOfChapters))
            print("Title of this book is"+ title)
            self.createBook(title,self.link,lstChapters)
            print("Book info:"+str(self.book))

    def saveBookHtml(self):
        self.data.saveBookToHtml(self.book)

    def cleanHtml(self, html):
        return self.parser.cleanDoc(html)

    def next(self):
         self.bot.clickNextChapter()

    def getMainContent(self):
        return self.bot.getContent()

scrappy = Scrappy()

enterUserCredentials = input("Would you like to enter new credentials ('y')")
if(enterUserCredentials == 'y'):
    scrappy.enterUserCredentials()

requestNewCookies = input("Would you like to request new cookies ('y')")
if(requestNewCookies == 'y'):
    scrappy.requestCookiesConnect()

#Main Loop
while input("Would you like to quit ('y')") != 'y':
    link = input("Enter the link which you want to scrap:\n")
    scrappy.scrapLink(link)
    
#End Main Loop
