#!/usr/bin/python
# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
from modules.image import Image

class Parser:

    def __init__(self):
        self.soup = None
        print("Init of Parser Module")
        
    def Soup(self, data):
        self.soup = BeautifulSoup(data, "html.parser")

    def cleanDoc(self, html):
        self.Soup(html)

        print("Cleaning Htlm")
        self.removeToolbar()
        self.removeRight()
        self.removeFooter()
        self.removeScripts()
        print("End of cleaning doc")

        self.soup.prettify()
        #print(self.soup.prettify)
        return str(self.soup.prettify())

    def removeToolbar(self):
        print("Deleting Toolbar")
        for div in self.soup.find_all("div", {"id": "Toolbar"}):
            div.decompose()

    def removeRight(self):
        print("Deleting Toolbar")
        for td in self.soup.find_all("td", {"class": "Right"}):
            td.decompose()

    def removeFooter(self):
        print("Deleting Footer")
        for div in self.soup.find_all("div", {"class": "Footer"}):
            div.decompose()

    def removeScripts(self):
        print("Removing script")
        for script in self.soup.find_all("script"):
            script.decompose()

    def updateImgSrc(self,html):
        self.Soup(html)
        try:
            print("Update of img srcs")
            for s in self.soup.find_all("img"):
                if (s["alt"]):
                    s["src"] = s["alt"]
            return str(self.soup.prettify()) 
        except:
            print("Trying to update img failed")
            return str(self.soup.prettify())

    def getChapterNumbers(self, html):
        print("Getting chapter number for this book")
        self.Soup(html)
        chapters = self.soup.find_all("a")
        return len(chapters)

    def getChapterList(self, html):
        print("Getting chapters list")
        self.Soup(html)
        chaptersLst = []
        for a in self.soup.find_all("a"):
            chaptersLst.append(a.text)
        return chaptersLst
    
    def extractImgs(self,html):
        i = 0
        imgs = []
        self.Soup(html)
        for img in self.soup.find_all("img"):
            print("img: "+img["src"])
            _img = Image()
            _img.id = i
            _img.src = img["src"]
            #if(img["alt"]):
             #   _img.alt = img["alt"]
            imgs.append(_img)
            i+=1
        return imgs
        
        
