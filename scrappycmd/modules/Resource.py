#!/usr/bin/python
# -*- coding: utf-8 -*-


class Resource:
    def __init__(self, r):
        self.id = r.id
        self.name = r.name
        self.url = r.url
