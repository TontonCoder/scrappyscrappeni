#!/usr/bin/python
# -*- coding: utf-8 -*-

from modules.image import Image

class Chapter:
    def __init__(self):
        self.id = None
        self.name = None
        self.url = None
        self.preHtml = ""
        self.postHtml = ""
        self.Images = []
    def addImage(self, i):
        self.Images.append(i)
