#!/usr/bin/python
# -*- coding: utf-8 -*-
from modules.Chapter import Chapter
from modules.Resource import Resource


class Book:
    def __init__(self, mainLink, name,chaptersLst):
        self.mainLink = mainLink
        self.name = name
        self.chaptersList = chaptersLst
        self.Chapters = []
        self.Resources = []

    def addChapter(self, chapter):
        self.Chapters.append(chapter)

    def addResource(self, resource):
        self.Resources.append(resource)
