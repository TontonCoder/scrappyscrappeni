#!/usr/bin/python
# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


class Bot:
    def __init__(self):
        self.connectUrl = "https://classrooms.doranco.fr/connexion"
        self.homepageUrl = "https://classrooms.doranco.fr/"
        self.gatewayUrl = "https://classrooms.doranco.fr/bibliotheque-numerique/"
        self.browser = None
        self.log = None
        print("Init of Bot Module")
        

    def connect(self, u, p):
        print("Connection started")

        self.init()
        self.browser.get(self.connectUrl)

        self.fillLogins(u,p)
        self.pressEnter()
        print("Waiting 5 seconds")
        time.sleep(5)                

    def init(self):
        print("Starting new browser ...")
        self.browser = webdriver.Firefox()

    def startscrapping(self, link):
        time.sleep(2)
        self.getRequest(link)
        
    def closeBrowser(self):
        self.browser.quit()
        self.browser = None
        print("Closing browser")

    def removeCookies(self):
        pass

    def injectCookies(self,cookies):
        print("Injecting Cookies ...")
        self.init()
        self.getRequest(self.homepageUrl)
        for c in cookies:
            self.browser.add_cookie(c)
        print("Injected Cookies.")
        self.getRequest(self.homepageUrl)
        time.sleep(3)
        self.getRequest(self.gatewayUrl)
        time.sleep(5)

    def currentContent(self):
        print("Current content extract")

    def getRequest(self, r):
        print("Request on: "+ r)
        self.browser.get(r)

    def extractCookies(self):
        print("Extracting Cookies ...")
        return self.browser.get_cookies()

    def fillUser(self, u):
        userInput = self.browser.find_element_by_id("user_login")
        userInput.send_keys(u)

    def fillPassword(self,p):
        passwordInput = self.browser.find_element_by_id("user_pass")
        passwordInput.send_keys(p)

    def pressEnter(self):
        submitBtn = self.browser.find_element_by_id("wp-submit")
        submitBtn.send_keys(Keys.RETURN)
        print("Form Validation")

    def fillLogins(self, u, p):
        self.fillUser(u)
        self.fillPassword(p)

    def clickNextChapter(self):
        self.findNextButton().click()
        time.sleep(1.5)

    def findNextButton(self):
        return self.browser.find_element_by_class_name("btnNext")

    def getContent(self):
        containerContent = self.browser.find_element_by_xpath("//div[@id='Content']")
        return containerContent.get_attribute("innerHTML")
        
    def getSummary(self):
        return self.browser.find_element_by_id("Summary").get_attribute("innerHTML")
        
    def getTitle(self):
        time.sleep(1)
        return self.browser.find_element_by_class_name("title").text    
        



