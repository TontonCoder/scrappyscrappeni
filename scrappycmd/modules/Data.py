#!/usr/bin/python
# -*- coding: utf-8 -*-
import dill
import os
import re
import requests
import pdfkit

class Data:
    def __init__(self):
        self.cookiesDir = "./cookies/"
        self.cookieFile = "save.pkl"
        self.booksDir = "./books/"
        self.userFile = "user.txt"
        print("Init of Data Module")
            
    
    def saveCookies(self, c):
        print("Saving cookies")
        if self.dirExists(self.cookiesDir) == False:
            os.mkdir(self.cookiesDir)
        with open(self.cookiesDir + self.cookieFile, "wb") as f:
            dill.dump(c, f)

    def getCookies(self):
        print("Loading Cookies")
        file = self.cookiesDir + self.cookieFile
        if self.fileExists(file):
            print("Cookies Exists")
            with open(file, "rb") as f:
                return dill.load(f)
        else:
            print("Cookies does not exists try first connect")
            return None

    def dirExists(self, dir):
        print("Checking directory exists..")
        return os.path.isdir(dir)

    def createDir(self, dir):
        print("Creating directory")
        os.mkdir(dir)

    def booksDirExists(self):
        if(self.dirExists(self.booksDir) == False):
            self.createDir(self.booksDir)
            print("created Directory:"+self.booksDir)

    def fileExists(self, path):
        print("Checking file exists")
        return os.path.exists(path)

    def getUser(self):
        print("Extracting User/Pwd")
        file = self.userFile
        if self.fileExists(file):
            return open(file, "r").read().split(";")
        else:
            return None

    def saveUser(self, user, password):
        print("Saving User/Password")
        file = self.userFile
        open(file, "w").write(user + ";" + password)
        print("Logins Saved")

    def downloadImg(self,dir,url, name):
        print("Downloading Image ...")

        _dir = dir
        r = requests.get(url)
        sizeFile = len(r.content)
        contentType = r.headers["content-type"].split("/")
        # name = dir + name + "." + contentType[1]
        name = _dir +  name
        leftToDownload = sizeFile
        print("Taille du fichier:", sizeFile)
        print("****Connecté****")
        f = open(name, "wb")
        print("Téléchargement.....")
        for chunk in r.iter_content(chunk_size=255):
            if chunk:  # filter out keep-alive new chunks
                # print(chunk)
                print("Restant: " + str(leftToDownload) + " / " + str(sizeFile))
                f.write(chunk)
                leftToDownload = leftToDownload - 255

        print("Fini.")
        print(contentType[1])

        f.close()
    def downloadResource(self):
        print("Downloading Resource ...")

    def saveBookToHtml(self, book):
        self.booksDirExists() #verify first if books directory exists
        _bookdir = re.sub('[^A-Za-z0-9]+','',book.name)
        bookDir = self.booksDir + _bookdir +"/"
        allImages = []
        uri = "https://www.eni-training.com/"
        
        imgDir = bookDir + "images/"

        if (self.dirExists(bookDir) == False):
            self.createDir(bookDir)
        if (self.dirExists(imgDir +"/")==False):
            self.createDir(imgDir)
        
        
        filename = "index.html"
        data =  ""
        

        for chapter in book.Chapters:
            if(chapter.name != "Quiz"):

                data = data + str(chapter.postHtml)
            #print("Images len " + str(len(chapter.Images)))
            #Attention je dois dabord récuperer les src de bases puis ensuite utiliser les src ceux d'origine on 
            # src=../download/sqdsdqg245df.png
            #sauf que je modifie déjà le chemin avant de les avoir copié
                if (len(chapter.Images)>0):

                    for img in chapter.Images:
                        if (img):
                            try:
                                tempsrc = img.src
                                tempsrc = tempsrc.replace("../", "")
                                name = tempsrc.split("/")[3].split("?")[0]
                                fullLink = uri + tempsrc
                                print("Full Link = "+ fullLink)
                            
                                self.downloadImg(imgDir+'/',fullLink, name)
                            except:
                                print("Exception on download img or split")
        
        #print(str(data))
        self.saveHtml(data, bookDir+filename)
        self.saveAsPdf(filename,bookDir)
        
    def saveHtml(self,data,filename):
        f = open(filename, "w", encoding="utf-8")
        htmlStart = """<?xml version="1.0" encoding="UTF-8" ?>
        <!DOCTYPE html>
        <body>
        """
        htmlEnding = """</body>
        </html>
        """
        f.write(htmlStart)
        f.write(data)
        f.write(htmlEnding)
        f.close()

    def saveAsPdf(self,filehtml,dir):
        try:
            pdffile = "book.pdf"
            if(self.fileExists(dir+'book.pdf')):
                pdffile = "1"+pdffile
            pdfkit.from_file(dir+filehtml,dir+pdffile)
        except:
            print("Exception to pdf")
            
        
        