# Scrappy Scrapp ENI

Pré-requis
- Mettre les deux drivers dans les variables d'environnment PATH (geckodriver & wkhtml) répertoire `drivers\`

Pour lancer la version python
il faut au préalable installer Python recommande >= 3.7.4
ajouter également PYTHON aux variables d'environnement

Après l'ajout des drivers dans les variables d'environnement un petit redémarrage est conseillé.

Lancer à partir de la racine du projet l'installation des packages via la commande `pip install -r requirements.txt`
puis lancer l'application via la commande `python scrappycmd.py`.
